<?php

namespace WPC\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;

if (!defined('ABSPATH')) exit; // Exit if accessed directly


class fess_wid extends Widget_Base
{

  public function get_name()
  {
    return 'advertisement';
  }

  public function get_title()
  {
    return 'کارمزدها';
  }

  public function get_icon()
  {
    return 'fa fa-camera';
  }

  public function get_categories()
  {
    return ['general'];
  }

  protected function _register_controls()
  {

    $this->start_controls_section(
      'section_content',
      [
        'label' => 'تنظیمات',
      ]
    );

    $this->add_control(
      'font_family',
      [
        'label' => esc_html__('Font Family', 'textdomain'),
        'type' => \Elementor\Controls_Manager::FONT,
        'default' => "'Open Sans', sans-serif",
        'selectors' => [
          '{{WRAPPER}} .fees' => 'font-family: {{VALUE}}',
        ],
      ]
    );


    $this->add_control(
      'title_coin',
      [
        'label' => 'عنوان کوین',
        'type' => \Elementor\Controls_Manager::TEXT,
        'default' => 'بیت کوین'
      ]
    );


    $this->add_control(
      'symbol_coin',
      [
        'label' => 'نماد کوین',
        'type' => \Elementor\Controls_Manager::TEXT,
        'default' => 'BTC'
      ]
    );

    $this->add_control(
      'image_coin',
      [
        'label' => 'عکس کوین',
        'type' => \Elementor\Controls_Manager::MEDIA,
        'default' => [
          'url' => '',
        ],
      ]
    );

    $this->add_control(
      'network_1',
      [
        'label' => 'شبکه',
        'type' => \Elementor\Controls_Manager::TEXT,
        'default' => ''
      ]
    );

    $this->add_control(
      'fee_1',
      [
        'label' => 'کارمزد',
        'type' => \Elementor\Controls_Manager::TEXT,
        'default' => ''
      ]
    );

    $this->add_control(
      'network_2',
      [
        'label' => 'شبکه',
        'type' => \Elementor\Controls_Manager::TEXT,
        'default' => ''
      ]
    );

    $this->add_control(
      'fee_2',
      [
        'label' => 'کارمزد',
        'type' => \Elementor\Controls_Manager::TEXT,
        'default' => ''
      ]
    );

    $this->add_control(
      'network_3',
      [
        'label' => 'شبکه',
        'type' => \Elementor\Controls_Manager::TEXT,
        'default' => ''
      ]
    );

    $this->add_control(
      'fee_3',
      [
        'label' => 'کارمزد',
        'type' => \Elementor\Controls_Manager::TEXT,
        'default' => ''
      ]
    );

    $this->add_control(
      'network_4',
      [
        'label' => 'شبکه',
        'type' => \Elementor\Controls_Manager::TEXT,
        'default' => ''
      ]
    );

    $this->add_control(
      'fee_4',
      [
        'label' => 'کارمزد',
        'type' => \Elementor\Controls_Manager::TEXT,
        'default' => ''
      ]
    );

    $this->end_controls_section();
  }


  protected function render()
  {
    $settings = $this->get_settings_for_display();

    $this->add_inline_editing_attributes('label_heading', 'basic');
    $this->add_render_attribute(
      'label_heading',
      [
        'class' => ['advertisement__label-heading'],
      ]
    );
?>
    <div class="fees">
      <div id="box">

        <div id="coin_orgin">
          <?php echo '<img  id="coin_img" src="' . $settings['image_coin']['url'] . '">' ?>
          <p id="coin_title"><?php echo $settings['title_coin']; ?> </br><span style="font-family:'Roboto';"><?php echo $settings['symbol_coin'] ?><span></p>
        </div>


        <div id="coin_det">
          <p id="coin_title" style="font-family: 'Roboto';"><?php echo $settings['network_1'] ?></br><?php echo $settings['fee_1'] ?></p>
          <p id="coin_title" style="font-family: 'Roboto';"><?php echo $settings['network_2'] ?></br><?php echo $settings['fee_2'] ?></p>
          <p id="coin_title" style="font-family: 'Roboto';"><?php echo $settings['network_3'] ?></br><?php echo $settings['fee_3'] ?></p>
          <p id="coin_title" style="font-family: 'Roboto';"><?php echo $settings['network_4'] ?></br><?php echo $settings['fee_4'] ?></p>
        </div>

      </div>
    </div>

    <style>
      ::-webkit-scrollbar {
        width: 90% !important;
        height: 10px;
      }

      /* Track */
      ::-webkit-scrollbar-track {
        box-shadow: inset 0 0 4px grey;
        border-radius: 10px;
      }

      /* Handle */
      ::-webkit-scrollbar-thumb {
        background: red;
        border-radius: 10px;
      }

      /* Handle on hover */
      ::-webkit-scrollbar-thumb:hover {
        background: #b30000;
      }

      .fees {
        display: flex;
        justify-content: center;
      }

      #box {
        width: 900px;
        height: 132px;
        border-radius: 24px;
        border: 1px solid #e9e7e7;
        display: flex;
        align-items: center;
        margin-bottom: 10px;
        max-width: 100%;
        padding: 20px;
      }


      #coin_orgin {
        display: flex;
        align-items: center;
      }

      #coin_img {
        padding-left: 8px;
        padding-right: 8px;
        padding-bottom: 11px;
        width: 25%;
      }

      #coin_title {
        text-align: center;
        padding-left: 20px;
        padding-right: 20px;
      }

      #coin_title:first-child {
        text-align: center;
        padding-left: 20px;
        border-left: solid 1px #ccc;
        padding-right: 20px;
      }

      #coin_orgin #coin_title {
        padding-left: 2px;
        padding-right: 2px;
      }

      #coin_det {
        display: flex;
        align-items: center;
        padding-left: 80px;
        overflow-x: auto;
        width: 67%;
      }

      @media only screen and (max-width: 600px) {
        #coin_det {
          display: flex;
          align-items: center;
          padding-left: 80px;
          overflow-x: auto;
          width: 67%;
          padding-top: 12px;

        }

        #coin_img {
          padding-left: 8px;
          padding-right: 8px;
          padding-bottom: 11px;
          width: 35%;
        }
      }
    </style>



<?php
  }
}
