<?php
    /*
    Plugin Name: Fess
    Plugin URI: http://beta98.ir/
    Description: این افزونه قسمت کارمزدها را به المنتور اضافه میکند.
    Version: 1
    Author: Benjamin Beta
    Author URI: http://beta98.ir/
    Text Domain: fess
    */
    
    namespace WPC;
    

    class Widget_Loader{
    
      private static $_instance = null;
    
      public static function instance()
      {
        if (is_null(self::$_instance)) {
          self::$_instance = new self();
        }
        return self::$_instance;
      }
    
    
      private function include_widgets_files(){
        require_once(__DIR__ . '/widgets/fees_coins.php');
      }
    
      public function register_widgets(){
    
        $this->include_widgets_files();
    
    \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Widgets\fess_wid());
    
      }
    
      public function __construct(){
        add_action('elementor/widgets/widgets_registered', [$this, 'register_widgets'], 99);
      }
    }
    
    // Instantiate Plugin Class
    Widget_Loader::instance();